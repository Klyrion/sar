package study;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {


        System.out.println("Привет, <username>!");
        System.out.println("Type a number to select one of the following: \n" +
                "[1]  - Посчитать среднее арифметическое массива.");
        System.out.println("[2] - Факториал числа.");
        System.out.println("[3] - Вывести цену за определенный товар опираясь на его код \n" +
                "(предусмотреть возможность введение неверного кода).");
        System.out.println("[4] - Вывести на экран информацию о том является ли целое число записанное в переменную n, " +
                "чётным либо нечётным.");
        System.out.println("[0] - выход.");
        System.out.println("Введите номер выбранного варианта и нажмите Enter...");
        Scanner scanChoice = new Scanner(System.in);        //System.in - принимает значения введенные с клавиатуры
        int selectedProgram = scanChoice.nextInt();

        switch (selectedProgram) {
            case 1:
                System.out.println("Введите несколько чисел через запятую, например: 1,1,4,7 ...");
                Scanner scanArray = new Scanner(System.in);
                String scanned = scanArray.nextLine();
                List listOfNumbers = new ArrayList();
                for (String number : scanned.split(",")) {
                    listOfNumbers.add(
                            Integer.parseInt(
                                    number.trim()
                            )
                    );
                }
                int arithmeticMean = ArithmeticMean.calculateArithmeticMean(listOfNumbers);
                System.out.println(
                        String.format(
                                "Для указанного ряда [%s] среднее арифметическое равно: %d",
                                scanned,
                                arithmeticMean
                        )
                );
                break;
            case 2:
                System.out.println("Введите число...");
                Scanner scanNum = new Scanner(System.in);
                int scannedNum = scanNum.nextInt();
                long factorial = calculateFactorial(scannedNum);
                System.out.println(
                        String.format("%d! = %d", scannedNum, factorial));
                break;
            case 3:
                System.out.println("Нипанятна ");
                break;
            case 4:
                System.out.println("Введите число...");
                Scanner scanNum1 = new Scanner(System.in);
                int n = scanNum1.nextInt();
                System.out.println(
                        String.format("Введенное чило %d -- %s", n, checkNumberOddOrEven(n)));
                break;
            case 0:
            default:
                System.out.println("Будь здоров, <username>!");

        }
    }

    /**
     * Calculates factorial for the specified number
     *
     * @param number {@code int} to calculate factorial
     * @return {@code int} calculated factorial
     */
    private static long calculateFactorial(int number) {
        long res = 1;
        for (int i=1; i<=number; i++){
            res = res * i;
        }
        return res;
    }

    /**
     * Check whether the number is odd or even;
     *
     * @param number {@code int} number to check
     * @return {@code String} "чётоное" if number is odd; Otherwise {@code String} " не чётоное"
     */
    private static String checkNumberOddOrEven(int number) {
        if (number % 2 == 0) {
            return ("чётное");
        } else {
            return ("не чётное");
        }
    }
}