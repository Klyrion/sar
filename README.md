Java Tasks Project
============

Description
-----------

This is "Hello World" Example for Java. And few more console features
project needs maven


Create a JAR file
-----------------

For pack the main class for package as a JAR file, execute the follow command: ::

  mvn package

Or
  
  mvn install


Run a JAR file
--------------

For run the JAR file packed, execute the follow command: ::

  java -jar qa-tasks-1.0.0.jar